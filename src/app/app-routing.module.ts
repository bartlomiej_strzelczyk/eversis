import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AccessComponent,
  HomeComponent,
  UserDataComponent
} from './components';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'user-data',
    component: UserDataComponent,
  },
  {
    path: 'access',
    component: AccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
