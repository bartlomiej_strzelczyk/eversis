import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {
  AccessComponent,
  HeaderComponent,
  HomeComponent,
  UserDataComponent
} from './components';
import { UserDataService } from './services';

@NgModule({
  declarations: [
    AppComponent,
    AccessComponent,
    HeaderComponent,
    HomeComponent,
    UserDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    UserDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
