import {
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';

import { UserDataService } from '../../services';

@Component({
  selector: 'access',
  templateUrl: './access.component.html',
})

export class AccessComponent implements OnInit {

  public accessRequested: boolean;
  public legalAge: boolean;
  public name: string;
  public surname: string;

  constructor(
    private userDataService: UserDataService,
    private changeDetector: ChangeDetectorRef
  ) {

  }

  ngOnInit(): void {
    const data = this.userDataService.getData();
    this.name = data.name;
    this.surname = data.surname;
    this.legalAge = data.age >= 18;
    this.changeDetector.markForCheck();
  }

  public access(): void {
    this.accessRequested = true;
  }

}
