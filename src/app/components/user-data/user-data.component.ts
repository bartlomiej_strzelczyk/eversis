import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms'

import { UserDataService } from '../../services';
import { UserData } from '../../models';

@Component({
  selector: 'user-data',
  templateUrl: './user-data.component.html',
})

export class UserDataComponent {

  @ViewChild('userDataForm')userDataForm: NgForm;

  public userData = new UserData();
  public progress = '0%';

  constructor(
    private userDataService: UserDataService,
    private router: Router
  ) {
  }

  public updateProgress(): void {
    let progress = 0;
    if (this.userDataForm.controls.age.valid) {
      progress += 33;
    }
    if (this.userDataForm.controls.name.valid) {
      progress += 34;
    }
    if (this.userDataForm.controls.surname.valid) {
      progress += 33;
    }
    this.progress = `${progress}%`;
  }

  public saveData(): void {
    this.userDataService.saveData(this.userData);
    this.router.navigate(['/access']);
  }

}
