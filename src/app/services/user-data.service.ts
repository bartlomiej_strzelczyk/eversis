import { Injectable } from '@angular/core';

interface IUserData {
  name: string;
  surname: string;
  age: number;
}

@Injectable()
export class UserDataService {

  constructor() {
  }

  public saveData(data: IUserData) {
    localStorage.setItem('userData', JSON.stringify(data));
  }

  public getData(): IUserData|null {
    const userDataSerialized = localStorage.getItem('userData');
    return userDataSerialized ? JSON.parse(userDataSerialized) : null;
  }

}
